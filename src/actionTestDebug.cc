#include "FFI.h"
#include "ActionFw_stub_win.h"
#include <stdio.h>
#include <cstring>
#include <Windows.h>

extern "C"{
	void HsStart();
	void HsEnd();
	typedef void(*HsFunPtr)(void);
}

extern "C"{
	struct Cagentpos {
		char* agentcstr;
		HsInt64 pos[2];
	};
}

const int ITERATIONS = 10000;

int main(int argc, const char* argv[])
{
	printf("Struct size as:   %u\n", sizeof(Cagentpos));
	printf("Pointer size as:   %u\n", sizeof(char*));
	printf("Int64 size as:   %u\n", sizeof(HsInt64));
	HsStart();
	HsFunPtr myAFW = initAFW();
	const char* jarl = "JARL";
	HsInt32 a2nofoes = 0;
	HsInt32 a2onefoe = 1;
	Cagentpos agent;
	char* agentName = "agent";
	agent.agentcstr = agentName;
	agent.pos[0] = 20;
	agent.pos[1] = 21;
	HsPtr a3nofoes = NULL;
	HsPtr a3onefoe = &agent;
	HsInt32 a4 = 2;
	HsInt32 a5 = 3;
	HsInt64 pos[2] = {10, 11};
	HsPtr a6 = pos;
	HsInt64 a7 = 4;
	HsInt64 a8 = 5;
	HsInt64 a9 = 6;
	HsPtr resnofoes, resonefoe;

	resnofoes = runAFW(myAFW, a2nofoes, a3nofoes, a4, a5, a6, a7, a8, a9);
	printf("Got [nofoes]: %s", resnofoes);
	for (int i = 0; i < 20; i++)
	{
		resonefoe = runAFW(myAFW, a2onefoe, a3onefoe, a4, a5, a6, a7, a8, a9);
		printf("Got [onefoe]: %s", resonefoe);
	}
	HsEnd();
	return 0;
};