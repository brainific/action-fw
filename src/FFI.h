#ifndef FFI_H
#define FFI_H

typedef unsigned int HsChar;  // on 32 bit machine
typedef int HsInt;
typedef __int32 HsInt32;
typedef __int64 HsInt64;
typedef unsigned int HsWord;

extern void hs_init(int *argc, char **argv[]);
extern void hs_exit(void);

// Ensure that we use C linkage for HsFunPtr 
#ifdef __cplusplus
extern "C"{
#endif

	typedef void(*HsFunPtr)(void);

#ifdef __cplusplus
}
#endif

typedef void *HsPtr;
typedef void *HsForeignPtr;
typedef void *HsStablePtr;

#define HS_BOOL_FALSE 0
#define HS_BOOL_TRUE 1
#endif // FFI_H