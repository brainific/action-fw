module Main where

import ActionFw
import Data.Int
import Control.Concurrent (threadDelay)
import Foreign.Ptr
import Foreign.Storable
import Foreign.C.String

main :: IO CString
main = do
    call <- initAFW
    threadDelay 1000000
    agtName <- newCString "agent"
    let agtPos = poke (agtName, (3,4)) 
    let ownPos = poke (1,2)
    res <- (initAFWHs call) (fromIntegral 5) agtPos (fromIntegral 6) (fromIntegral 7) ownPos (fromIntegral 8) (fromIntegral 9) (fromIntegral 10)
    print res
    return res

-- Only thing remaining is the new action API!!!!
-- Int32 -> Ptr CAgentPos -> Int32 -> Int32 -> Ptr Pos -> Int64 -> Int64 -> Int64