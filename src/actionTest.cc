#include "FFI.h"
#include "ActionFw_stub_win.h"
#include <stdio.h>
#include <cstring>
#include <Windows.h>

extern "C"{
	void HsStart();
	void HsEnd();
	typedef void(*HsFunPtr)(void);
}

extern "C"{
	struct Cagentpos {
		char* agentcstr;
		HsInt64 pos[2];
	};
}

const int ITERATIONS = 10000;

int main(int argc, const char* argv[])
{
	printf("Struct size as:   %u\n", sizeof(Cagentpos));
	printf("Pointer size as:   %u\n", sizeof(char*));
	printf("Int64 size as:   %u\n", sizeof(HsInt64));
	HsStart();
	HsFunPtr myAFW = initAFW();
	const char* jarl = "JARL";
	HsInt32 a2 = 1;
	// CAgentPos = (AgentCStr, Pos)
	Cagentpos agent;
	char* agentName = "agent";
	agent.agentcstr = agentName;
	agent.pos[0] = 20;
	agent.pos[1] = 21;
	HsPtr a3 = &agent;
	// printf("Agent (a3) as:   %p\n", a3);
	// printf("Agentptr as:   %p\n", &(agent));
	// printf("Agentnameptr as:   %p\n", &(agent.agentcstr));
	// printf("Agentposptr as:   %p\n", &(agent.pos));
	// printf("Agentposptr0 as:   %p\n", &(agent.pos[0]));
	// printf("Agentposptr1 as:   %p\n", &(agent.pos[1]));
	// printf("Agentcstr as:   %p\n", agent.agentcstr);
	// printf("Agentname as:   %p\n", agentName);
	HsInt32 a4 = 2;
	HsInt32 a5 = 3;
	//-- Pos = (Int64, Int64)
	HsInt64 pos[2] = {10, 11};
	// printf("Pos0 as:   %p\n", pos);
	// printf("Pos1 as:   %p\n", &(pos[1]));
	HsPtr a6 = pos;
	HsInt64 a7 = 4;
	HsInt64 a8 = 5;
	HsInt64 a9 = 6;
	HsPtr res;

	LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
	LARGE_INTEGER Frequency;

	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);


	for (int i = 0; i < ITERATIONS; i++)
	{
		res = runAFW(myAFW, a2, a3, a4, a5, a6, a7, a8, a9);
		if (strcmp("ping!!!", (const char*)res) != 0)
		{
			printf("Error at iteration %u- %s\n", i, res);
			break;
		}
		// printf("Iteration %u\n", i);
	}

	QueryPerformanceCounter(&EndingTime);
	ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
	ElapsedMicroseconds.QuadPart *= 1000000;
	ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;

	printf("Elapsed time (us): %lld\n", ElapsedMicroseconds.QuadPart);
	printf("Time per iteration (us): %lld\n", ElapsedMicroseconds.QuadPart/ITERATIONS);

	HsEnd();
	return 0;
};