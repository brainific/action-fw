module ActionLogic where

-- This module specifies some kind of logic language
-- Let's start with FOL

-- Internal or BB states; we will provide a more detailed
-- type later on
data Predicate = String
type ModalOp = String
type Var = String

-- TAL solution: occurs (actions) + holds/occludes (states)

-- Modals: time knowledge (branching?) 1st approach
--         agent knowledge 2nd approach
--         public annoucements 3rd approach

data Formula = PredF Predicate |
               AndF Formula Formula |
               OrF Formula Formula |
               ImplyF Formula Formula |
               IffF Formula Formula |
               ExistsF Var Formula |
               ForallF Var Formula |
               ModalF ModalOp Formula

check :: Formula -> Bool
check (ExistsF var formula) = 
    True
check (ForAllF var formula) = 
    True
check _ =
    True