#include "FFI.h"
#ifdef __cplusplus
extern "C" {
#endif
extern HsFunPtr initAFW(void);
extern HsPtr runAFW(HsFunPtr a1, HsInt32 a2, HsPtr a3, HsInt32 a4, HsInt32 a5, HsPtr a6, HsInt64 a7, HsInt64 a8, HsInt64 a9);
// extern HsPtr ActionFw_d2sG(StgStablePtr the_stableptr, void* original_return_addr, HsInt32 a1, HsPtr a2, HsInt32 a3, HsInt32 a4, HsPtr a5, HsInt64 a6, HsInt64 a7, HsInt64 a8);
#ifdef __cplusplus
}
#endif

